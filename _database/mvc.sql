-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Verzió:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for tábla mvc.comment
CREATE TABLE IF NOT EXISTS `comment` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(200) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `comment_text` text COLLATE utf8_hungarian_ci,
  PRIMARY KEY (`c_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- Dumping data for table mvc.comment: 1 rows
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` (`c_id`, `nickname`, `email`, `comment_text`) VALUES
	(3, 'Dani', 'valami@semmi.hu', 'Ez az első komment. Ha van véleményed akkor írd meg bátran!');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;

-- Dumping structure for tábla mvc.teachers
CREATE TABLE IF NOT EXISTS `teachers` (
  `teacher_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `teacher_name` varchar(200) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `object` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  PRIMARY KEY (`teacher_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- Dumping data for table mvc.teachers: 3 rows
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` (`teacher_id`, `teacher_name`, `object`) VALUES
	(1, 'Kiss Lajos', 'matematika'),
	(2, 'Demeter Jolán', 'magyar'),
	(3, 'Kovács János', 'számtech');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;

-- Dumping structure for tábla mvc.teacher_profile
CREATE TABLE IF NOT EXISTS `teacher_profile` (
  `teacher_id` int(11) DEFAULT NULL,
  `birth` int(11) DEFAULT NULL,
  `sex` varchar(50) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `address` varchar(150) COLLATE utf8_hungarian_ci DEFAULT NULL,
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- Dumping data for table mvc.teacher_profile: 3 rows
/*!40000 ALTER TABLE `teacher_profile` DISABLE KEYS */;
INSERT INTO `teacher_profile` (`teacher_id`, `birth`, `sex`, `address`) VALUES
	(1, 1975, 'férfi', '1111 Budapest, Huszman Alajos utca 23.'),
	(2, 1980, 'nő', '1111 Budapest, Mártírok tere 52.'),
	(3, 1988, 'férfi', '4400 Nyíregyháza, Ferenc krt. 43.');
/*!40000 ALTER TABLE `teacher_profile` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
