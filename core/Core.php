<?php


class Core{
    
    
    public function __construct()
    {


        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/',$url);

        // ha kaptunk url paramétert, akkor
        // betöltjük a controllert és az index() metódust
        if(!empty($url[0]))
        {
            $file = 'controller/'.$url[0] . '.php';

            if(file_exists($file))
            {
                $controller = $url[0];
                include_once $file;
                
                //példányosítjuk a controllert
                $c = new $controller();

                //metódus hívás: ha kaptunk ok, ha nem akkor az index metódust hívjuk meg
                if(!empty($url[1]))
                {
                    $method = $url[1];
                    
                    if(method_exists($c,$method))
                    {
                        if(!empty($url[2]))
                            $c->$method($url[2]);
                        else
                            $c->$method();
                    }
                    else
                    {
                        echo "Nem létezik ez a metódus!";
                    }
                    
                }
                else
                {
                    $c->index();
                }

            }
        
        }

        // ha nem kaptunk url paramétert, 
        // akkor a kezdőlapot töltjük be
        else
        {
            include_once 'controller/Index.php';
            $c = new Index();
            $c->index();
        }
        
    } // end constructor
    
} // end class