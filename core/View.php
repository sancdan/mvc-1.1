<?php


class View{
    
    
    public function __construct(){
        // no code here
    }
    
    
    public function render($name){
        
        require_once 'view/common/header.php';
        require_once 'view/'.$name.'.php';
        require_once 'view/common/footer.php';
        
    }
    
    
}