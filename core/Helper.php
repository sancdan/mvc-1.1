<?php

function site_url($param = null)
{
    if($param){
        return 'http://'.$_SERVER['HTTP_HOST'].'/'.$param;
    }
    else{
        return 'http://'.$_SERVER['HTTP_HOST'].'/';
    }
}


function pr($param)
{
    echo '<pre>';
    print_r($param);
    die;
}