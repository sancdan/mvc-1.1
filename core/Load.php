<?php

class Load
{

    public function __construct(){
        // no code here
    }

    public function model($modelName = null)
    {
        $ucfirstModelName = ucfirst($modelName);
        require_once 'model/'.$modelName.'.php';
        return $this->$modelName = new $ucfirstModelName();
    }


}