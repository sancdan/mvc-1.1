<?php


class Teacher_m extends Model{


	public function __construct()
	{
		parent::__construct();
	}


	public function getTeachers()
	{
		$query = $this->db->prepare('select * from teachers order by teacher_name');
		$query->execute();
		return $query->fetchAll();
	}

	public function getTeacher($teacherId)
	{
		$query = $this->db->prepare('select teachers.*, teacher_profile.* from teacher_profile join teachers using(teacher_id) where teacher_id = :teacherId');
		$query->bindValue(':teacherId', $teacherId, PDO::PARAM_INT);
		$query->execute();
		return $query->fetch();
	}



}