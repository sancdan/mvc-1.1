<?php


class Comment_m extends Model{


	public function __construct()
	{
		parent::__construct();
    }
    


    public function insert()
    {
        $query = $this->db->prepare('insert into comment set nickname = :nickname, email = :email, comment_text = :commentText');
        $query->bindValue(':nickname', $_POST['nickname'], PDO::PARAM_STR);
        $query->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $query->bindValue(':commentText', $_POST['comment_text'], PDO::PARAM_STR);
        $query->execute();
    }


    public function getComments()
    {
        $query = $this->db->prepare('select * from comment order by c_id desc');
        $query->execute();
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }



}