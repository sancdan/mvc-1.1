        <!-- Image Section - set the background image for the header in the line below -->
        <section class="py-5 mt-5 bg-image-full" style="background-image: url('https://unsplash.it/1900/1080?image=1081');">
        <!-- Put anything you want here! There is just a spacer below for demo purposes! -->
        <div style="height: 200px;"></div>
        </section>
        
        <footer class="py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">Copyright &copy; PHP :: MVC | 2018</p>
            </div>
            <!-- /.container -->
        </footer>

        <!-- bootstrap -->
        <script src="assets/bootstrap-4-1-3/js/bootstrap.min.js"></script>

    </body>


</html>