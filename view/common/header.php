<!doctype html>

<html>

    <head>
        <base href="<?php echo site_url() ?>">

    	<meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- bootstrap css -->
        <link rel="stylesheet" href="assets/bootstrap-4-1-3/css/bootstrap.min.css">
		
        <!-- custom css -->
		<link rel="stylesheet" href="public/css/mvcstyle.css">

        <!-- jquery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!-- fontawesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


    </head>
    
    
    <body>


    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="<?php echo 'http://'.$_SERVER['HTTP_HOST']; ?>">PHP :: MVC</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?php echo 'http://'.$_SERVER['HTTP_HOST']; ?>">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?url=comment">Comment</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Help
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="?url=help">General help</a>
                <a class="dropdown-item" href="?url=help/sos">SOS</a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    

    <!-- Header - set the background image for the header in the line below -->
    <header class="py-5 bg-image-full" style="background-image: url('https://unsplash.it/1900/1080?image=1076');"></header>