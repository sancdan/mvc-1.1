		
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

				<h1 class="mt-5">Tanárok</h1>
				<h3>Tanárok listája</h3>
				<ul>
					<?php foreach($this->teachers as $teacher): ?>
					<li>
						<?php echo $teacher['teacher_name']; ?> (<?php echo $teacher['object']; ?>) - 
						[<a href="?url=teachers/profile/<?php echo $teacher['teacher_id'] ?>">profil</a>]
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
        </div>
    </div>
