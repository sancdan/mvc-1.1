
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
				<h1 class="mt-5">Tanárok</h1>
				<h3>Tanár adatai</h3>
				<ul>
					<li><strong>Név:</strong> <?php echo $this->teacher['teacher_name'] ?></li>
					<li><strong>Tantárgy:</strong> <?php echo $this->teacher['object'] ?></li>
					<li><strong>Szül. év:</strong> <?php echo $this->teacher['birth'] ?></li>
					<li><strong>Nem:</strong> <?php echo $this->teacher['sex'] ?></li>
					<li><strong>Cím:</strong> <?php echo $this->teacher['address'] ?></li>
				</ul>
			</div>
        </div>
    </div>
