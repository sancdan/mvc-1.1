
<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="mt-5">Írj egy kommentet ha akarsz!</h1>
				<p class="lead">
					De nem fontos, csak ha kedved van... :)
				</p>
			</div>

            <div class="col-lg-12">

                <?php if(isset($this->data['error'])): ?>
                <div class="alert alert-danger"><?php echo $this->data['error']; ?></div>
                <?php else:?>
                <div class="alert alert-warning">Minden mező kitöltése kötelező.</div>
                <?php endif;?>

                <form action="?url=comment/send" method="post" name="commentForm" accept-charset="utf-8">
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name="nickname" class="form-control" value="<?php echo isset($this->data['nickname']) ? $this->data['nickname'] : '' ?>" placeholder="Nickname">
                        </div>
                        <div class="col">
                            <input type="email" name="email" class="form-control" value="<?php echo isset($this->data['email']) ? $this->data['email'] : '' ?>" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-row mt-5">
                        <div class="col">
                            <textarea name="comment_text" class="form-control" rows="5" placeholder="Write your comment here..."><?php echo isset($this->data['comment_text']) ? $this->data['comment_text'] : '' ?></textarea>
                        </div>
                    </div>

                    <div class="form-row mt-5">
                        <div class="col">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-comment-dots"></i> Send</button>
                            </div>
                        </div>
                    </div>
                </form>                
            
            </div>

		</div>
	</div>

    <hr>

    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <h3><i class="fas fa-comments"></i> Eddigi hozzászólások:</h3>

                <?php foreach($this->comments as $comment): ?>
                <div class="pt-4 pb-2 border-bottom">
                    <p><strong><?php echo $comment['nickname']; ?></strong> <span class="badge badge-info"><?php echo $comment['email'] ?></span></p>
                    <p><?php echo $comment['comment_text']; ?></p>
                </div>
                <?php endforeach ?>

            </div>
        </div>


    </div>