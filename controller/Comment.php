<?php


class Comment extends Controller{
    
    
    public function __construct(){
        
        parent::__construct();

        //load model
        $this->comment_m = $this->load->model('comment_m');
        
    }


    public function index()
    {
        $this->view->comments = $this->comment_m->getComments();
    	$this->view->render('comment/index');
    }


    // komment adatbázisba küldése
    public function send()
    {
        if(
            !empty($_POST['nickname']) &&
            !empty($_POST['email']) &&
            !empty($_POST['comment_text'])
        ){
            $this->comment_m->insert();
            header('Location: '.site_url('?url=comment'), TRUE, 307);
        }
        else{
            $data['error'] = 'Mondtam <i class="fas fa-grin"></i>';
            $data['nickname'] = $_POST['nickname'];
            $data['email'] = $_POST['email'];
            $data['comment_text'] = $_POST['comment_text'];

            $this->view->data = $data;

            $this->view->comments = $this->comment_m->getComments();

            $this->view->render('comment/index');
        }
    }

    
}