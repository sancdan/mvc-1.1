<?php


class Help extends Controller{
    
    
    public function __construct(){
        
        parent::__construct();
        
        //echo "Ez itt a " . __CLASS__ . " controller.";
        
        
    }


    public function index(){
    	$this->view->render('help/help');
    }

    
    
    public function sos(){
        $this->view->render('help/sos');
    }
    
}