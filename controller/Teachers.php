<?php


class Teachers extends Controller{

	public function __construct()
	{
        parent::__construct();

        //load model
        $this->teacher_m = $this->load->model('teacher_m');

        

    }


    public function index()
    {
    	$this->view->teachers = $this->teacher_m->getTeachers();
    	$this->view->render('teachers/teachers');
    }



    public function profile($teacherId)
    {
        $this->view->teacher = $this->teacher_m->getTeacher($teacherId);
        //var_dump($this->view->teacher);
        $this->view->render('teachers/profile');
    }



}